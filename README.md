# Homebrew package for [Geographic Network Tools](https://gitlab.com/shodan-public/geonet-rs)

<p>
<a href="https://opensource.org/licenses/MIT"><img src="https://img.shields.io/badge/license-MIT-_red.svg"></a>
<a href="https://twitter.com/shodanhq"><img src="https://img.shields.io/twitter/follow/shodanhq.svg?logo=twitter"></a>
</p>

The package currently supports these Apple architectures: **x86_64**, **arm64**.

## Installation

Tap repository which hosts `geonet.rb` formula has been moved to this [repo](https://gitlab.com/shodan-public/homebrew-shodan)

```shell
brew tap shodan-public/homebrew-shodan https://gitlab.com/shodan-public/homebrew-shodan
brew install geonet
```
